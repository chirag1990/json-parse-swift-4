//
//  CourseModel.swift
//  JsonParseSwift4
//
//  Created by Chirag Tailor on 18/01/2018.
//  Copyright © 2018 Chirag Tailor. All rights reserved.
//

import Foundation

struct Course : Decodable {
    let id : Int
    let name : String
    let link : String
    let imageUrl : String
    
    //required for swift 2/ swift 3/ objc
//    init(json : [String:Any]) {
//        id = json["id"] as? Int ?? -1
//        name = json["name"] as? String ?? ""
//        link = json["link"] as? String ?? ""
//        imageUrl = json["imageUrl"] as? String ?? ""
//    }
}
