//
//  WebsiteDescriptionModel.swift
//  JsonParseSwift4
//
//  Created by Chirag Tailor on 18/01/2018.
//  Copyright © 2018 Chirag Tailor. All rights reserved.
//

import Foundation

struct WebsiteDescription : Decodable {
    let name: String
    let description : String
    let courses: [Course]
}
