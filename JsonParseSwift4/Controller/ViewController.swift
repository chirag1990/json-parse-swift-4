//
//  ViewController.swift
//  JsonParseSwift4
//
//  Created by Chirag Tailor on 18/01/2018.
//  Copyright © 2018 Chirag Tailor. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        CourseService.instance.getCourseData { (success) in
            if success {
                // Do your stuff here with the data
                                
                print("getCourseData Success")
            } else {
                print("getCourseData fail")
            }
        }
        
        CourseService.instance.getMultipleCoursesData { (success) in
            if success {
                // Do your stuff here with the data
                print("getMultipleCoursesData Success")
            } else {
                print("getMultipleCoursesData fail")
            }
        }
        
        CourseService.instance.getWebsiteDescription { (success) in
            if success {
                // Do your stuff here with the data
                print("getWebsiteDescription Success")
            } else {
                print("getWebsiteDescription fail")
            }
        }
    }
}

