//
//  CourseService.swift
//  JsonParseSwift4
//
//  Created by Chirag Tailor on 18/01/2018.
//  Copyright © 2018 Chirag Tailor. All rights reserved.
//

import Foundation

class CourseService {
    
    static let instance = CourseService()
    
    
    // Single Course
    
    func getCourseData(completion: @escaping CompletionHandler) {
        
        guard let url = URL(string: JSON_URL_STRING) else { return }
        
        URLSession.shared.dataTask(with : url) {(data, response, err) in
            
            guard let data = data else {return}
            
            // let dataAsString = String(data: data, encoding : .utf8)
            
            do {
                let course =  try JSONDecoder().decode(Course.self, from: data)
                completion(true)
                print(course.name)
            } catch {
                completion(false)
                print("Error Serializing json:")
            }
            
            //            do {
            //
            //                //swift 2/ 3/ objc
            //                guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else {return}
            //
            //                let course = Course(json: json)
            //                completion(true)
            //                print(course)
            //
            //
            //            }catch let jsonErr {
            //                completion(false)
            //                print("Error Serializing json:", jsonErr)
            //            }
            
            }.resume()
    }
    
    func getMultipleCoursesData(completion: @escaping CompletionHandler) {
        guard let url = URL(string : JSON_COURSE_URL) else {return}
        
        URLSession.shared.dataTask(with: url) {(data, response, err) in
            
            guard let data = data else {return}
            
            do {
                let courses = try JSONDecoder().decode([Course].self, from: data)
                completion(true)
                print(courses)
            } catch {
                completion(false)
                print("Error Serializing json:")
            }
            
        }.resume()
    }
    
    func getWebsiteDescription(completion: @escaping CompletionHandler) {
        guard let url = URL(string : JSON_WEBSITE_DESC_URL) else {return}
        
        URLSession.shared.dataTask(with: url) {(data, response, err) in
            
            guard let data = data else {return}
            
            do {
                let websiteDescription = try JSONDecoder().decode(WebsiteDescription.self, from: data)
                completion(true)
                print(websiteDescription.name, websiteDescription.description)
            } catch {
                completion(false)
                print("Error Serializing json:")
            }
            
        }.resume()
    }
    
}
