//
//  Constant.swift
//  JsonParseSwift4
//
//  Created by Chirag Tailor on 18/01/2018.
//  Copyright © 2018 Chirag Tailor. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success: Bool) -> ()
let JSON_URL_STRING = "https://api.letsbuildthatapp.com/jsondecodable/course"
let JSON_COURSE_URL = "https://api.letsbuildthatapp.com/jsondecodable/courses"
let JSON_WEBSITE_DESC_URL = "https://api.letsbuildthatapp.com/jsondecodable/website_description"
